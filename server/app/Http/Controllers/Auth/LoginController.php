<?php

namespace App\Http\Controllers\Auth;

use App\Http\Resources\User as UserResource;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use App\Models\User;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(LoginRequest $request) {
        try {
            $user = User::where('email', $request->email)->first();
            $deviceName = $request->device_name;
            if (! $user || ! Hash::check($request->password, $user->password)) {
                throw ValidationException::withMessages([
                    'message' => 'The provided credentials are incorrect.',
                ]);
            }
            $token = $user->createToken($deviceName !== null ? $deviceName : $request->email)->plainTextToken;
            return $this->responseWithToken($token, $user);
        } catch(Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ]);
        }
    }

    protected function responseWithToken($token, $user) {
        return response()->json([
            'success'     => true,
            'token'       => $token,
            'token_type'  => 'bearer',
            'user'        => new UserResource($user)
        ], JsonResponse::HTTP_OK);
    }

    protected function incorrectCredentials($error) {
        return response()->json([
            'success'   => false,
            'message'   => $error
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
