<?php

namespace App\Http\Controllers\Auth;

use App\Http\Resources\User as UserResource;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Str;
use App\Models\User;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  App\Http\Requests\RegisterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request) {
        $userData = $request->merge([
            'password'  => Hash::make($request->password),
        ])->only('name', 'phone_number', 'email', 'password');

        // $userData['username'] = $request->username !== null
        //     ? Str::slug($request->username)
        //     : Str::slug($request->name) . '-' . Str::random(3);

        $user = User::create($userData);
        $token = $user->createToken($user->email)->plainTextToken;;
        return $this->responseWithToken($token, $user);
    }

    protected function responseWithToken($token, $user) {
        return response()->json([
            'success'     => true,
            'token'       => $token,
            'token_type'  => 'bearer',
            'user'        => new UserResource($user)
        ], JsonResponse::HTTP_CREATED);
    }
}
