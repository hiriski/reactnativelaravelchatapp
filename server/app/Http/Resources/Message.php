<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Message extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            '_id'       => $this->id,
            'text'      => $this->body,
            'createdAt' => $this->created_at,
            // 'user'      => new User($this->whenLoaded('user')),
            'user'      => new User($this->user),
            'send'      => $this->send,
            'received'  => $this->received,
        ];
    }
}
