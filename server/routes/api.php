<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\MessageController;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Broadcast::routes(['middleware' => ['auth:sanctum']]);


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



/*
|--------------------------------------------------------
| Authentication Routes
|--------------------------------------------------------
*/
Route::prefix('/auth')->group(function() {
    Route::post('/register', RegisterController::class);
    Route::post('/login', LoginController::class);

    /** Get authenticated user */
    Route::get('/get-authenticated-user', AuthController::class);
});


/*
|--------------------------------------------------------
| Chats Routes
|--------------------------------------------------------
*/
Route::apiResource('/message', MessageController::class)->only(['index', 'store']);