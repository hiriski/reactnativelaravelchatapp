import React, {useState, useCallback, useEffect} from 'react';
import {GiftedChat} from 'react-native-gifted-chat';
import api from './api';
import {useChat} from './context';
import {pusher} from './App';

const Chat = () => {
  const {messages, setMessages} = useChat();
  console.log(messages);

  const fetchMessage = async () => {
    await api
      .get('/api/message')
      .then(response => {
        if (response.status === 200) {
          setMessages(response.data.data);
        }
      })
      .catch(e => console.log(e));
  };

  useEffect(() => {
    fetchMessage();
    return () => {
      console.log('CLEAN UP');
    };
  }, []);

  React.useEffect(() => {
    var channel = pusher.subscribe('chat');
    channel.bind('chat-event', function (data) {
      console.log(data);
    });
  }, []);

  const onSend = useCallback((newMessages = []) => {
    api
      .post('/api/message', {body: newMessages[0].text})
      .then(response => {
        if (response.status === 201) {
          setMessages(previousMessages => {
            GiftedChat.append(previousMessages, response.data.data);
            return [...previousMessages, response.data.data];
          });
        }
      })
      .catch(e => console.log(e.response));
  }, []);

  return (
    <GiftedChat
      infiniteScroll
      showUserAvatar={true}
      messages={messages}
      onSend={messages => onSend(messages)}
      user={{
        _id: 3,
        name: 'An Riski',
      }}
    />
  );
};

export default Chat;
