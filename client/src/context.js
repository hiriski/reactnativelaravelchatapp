import React, {createContext, useState, useContext} from 'react';
import PropTypes from 'prop-types';

export const ChatContext = createContext();

export const ChatContextProvider = ({children}) => {
  const [messages, setMessages] = useState([]);
  return (
    <ChatContext.Provider value={{messages, setMessages}}>
      {children}
    </ChatContext.Provider>
  );
};

export const useChat = () => {
  return useContext(ChatContext);
};

ChatContextProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ChatContextProvider;
