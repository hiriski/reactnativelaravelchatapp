import React, {useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Pusher from 'pusher-js/react-native';
import Chat from './Chat';
import ChatContextProvider from './context';

// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;

export const pusher = new Pusher('abc123', {
  forceTLS: false,
  cluster: 'ap1',
  wsHost: '192.168.43.154',
  wsPort: 6001,
  disableStats: true,
});

const App = () => {
  useEffect(() => {
    // var channel = pusher.subscribe('chat');
    // channel.bind('chat-event', function (data) {
    //   alert(JSON.stringify(data));
    // });
  }, []);
  return (
    <ChatContextProvider>
      <View style={styles.root}>
        <Chat />
      </View>
    </ChatContextProvider>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#fbfbfb',
  },
});

export default App;
