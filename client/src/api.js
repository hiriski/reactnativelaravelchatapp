import axios from 'axios';

const API_URL = 'http://192.168.43.154:8000'; // default artisan serve
const AUTH_TOKEN = '5|2nN5J9K53Xm6Jjzl2YE22fNOnJe4hGOBcSDcFHv1'; // sample token

axios.defaults.baseURL = API_URL;

const apiInstance = axios.create();

apiInstance.interceptors.request.use(
  config => {
    config.headers.Authorization = `Bearer ${AUTH_TOKEN}`;
    return config;
  },
  error => {
    return Promise.reject(error);
  },
);

export default apiInstance;
